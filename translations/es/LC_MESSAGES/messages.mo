��    �      �              �  1   �     �     �           	          !  	   2  
   <  +   G  &   s     �     �     �     �  *   �          )     0     B     G  -   N  *   |     �  
   �     �     �     �     �  #        )     6     G     O     b     o     v     �     �     �     �     �     �     �     �     �  
   �             9   "     \  *   p     �     �     �  -   �     �     �               #     *     ;  t   A  3   �  2   �          +     G     P     V     ]     o     �     �     �  
   �     �     �     �     �     �     �          !     =     Q     f     r          �     �  
   �     �  !   �     �     �               4     D     K     P     W  
   m     x     �     �  &   �     �     �                    (     5     E     M     S     [     s     {      �  d   �     	  `   "  (   �  0   �     �     �       
      
   +     6     ;     H     h     w     �     �     �     �     �     �      �  Y        u     �     �     �     �  %   �  *   �          &     >     V     b  +   t     �     �     �     �     �                /  
   A  
   L     W  "   w     �     �     �  
   �  	   �     �     �                     #     @     `  e  m  1   �               &     5     F     M  	   ^  
   h  +   s  &   �     �     �     �     �  *        <     U     \     q     x  4   �  *   �     �  	   �     �               3  #   @     d     q     �     �     �     �     �     �     �     �     �     �                    7     L     [     i  9   �     �  *   �  !   �               -   0      ^      d      r      �      �      �      �   t   �   3   #!  2   W!     �!     �!     �!     �!     �!     �!     �!     �!     "     "  	   "     "     &"     8"     W"     d"     u"     �"  )   �"     �"     �"     �"     #     #     .#     A#     G#     U#  !   n#     �#     �#     �#     �#     �#     �#     �#     �#     �#     $     $     5$     E$  5   [$     �$     �$     �$     �$     �$     �$     �$  	   
%     %     %     !%     9%     A%     J%  d   [%     �%  `   �%  6   :&  9   q&     �&     �&     �&  
   �&     '     '     "'     ;'     ['     v'     �'     �'     �'     �'     �'     �'      	(  Y   *(     �(     �(  	   �(  
   �(     �(  %   �(  *   �(     ))     <)     T)  
   l)     w)  +   �)     �)     �)     �)  m   �)  m   X*  e   �*  �   ,+  �   �+    �,  	   �.  �   �.  :  �/  �  �0     �4     �4  �   �4     �5     �5     �5     �5  �   �5     �6    �6  �   �7  �  f8    If you don't receive it, check your Spam folder! %s removed OK 404 Page not found About us Activate account Active Add a new member Add group Add member Add or remove people from these collectives Add or remove people from these groups Add user Added %s Already exist in the database Already got an account and An account with this email already exists. Attribute '%s' not found Avatar Avatar changed OK Body Cancel Cannot delete. User has been exported to LDAP Cannot remove user from Primary collective Change password Collective Collective members Collectives Collectives you manage CommonsCloud Confirm your email at Commons Cloud Create group Create new group Created Credentials needed DN not valid Delete Delete entry Description Does not exist Edit Edit avatar Edit profile Edit your profile Email Email address Email confirmed OK Email sent Email sent OK Emails do not match Error connecting to Mailtrain. More info on stderr logs.  Failed to create %s Finish onboarding process at Commons Cloud Forgotten your password? From Fullname Grant or deny people access to these services Group Group created Group management Group members Groups Groups of people Hello I accept the <a href='%s' target='_blank'>conditions of use</a> and <a href='%s' target='_blank'>privacy policy.</a> I want to subscribe to the Commonscloud newsletter. If the user is not found, send this link via email Image formats Insufficient permission: %s Language Login Logout Looking for help? Maintenance mode Manage groups for: Managers Members My profile Name New password New user requesting an account No file part No selected file Notify manager Onboarding complete Onboarding not yet finished Password changed OK Passwords must match Please edit Please login Please set your new password Preferred language Quota Recipients Recover password Recover password at Commons Cloud Register Remove user Remove user from group Repeat email address Repeat password Return Save Search Search for the person Send email Send email again Send email to Send email to members Send the user an email to set password Sent activation email Sent email to manager(s) Service Services Services you manage Set password Short biography Sign up State Subject Subscribe to newsletter Support Surname Tell us something about yourself Thank you for confirming your email address. We'll send you another email to configure your password The new username will be The onboarding process for CommonsCloud goes as follows. The clock icon tells you where you are! There were fleas last summer in Menorca. Think of a longer, easier to remember passphrase This can't be undone This element already exists. Unable to update Unattended Updated OK User User deleted User is already in the LDAP DIT User not found User not found: %s Username Username is already taken Users Users you manage Valid credentials needed Visit our community support! Waiting for user to set password We sent you the first of two emails. In this one we ask you to comfirm your email address We've sent you an email Website Websites Welcome You can manage these groups You can manage your user profile here You can now access your CommonsCloud tools You may login here You will be sent a copy Your account is created Your groups Your new username Your password has been changed successfully Your username Your username is Your websites _confirmEmailAddress_email_text _confirmEmail_text _newUserWaiting_email_text _resetPassword_text _setPassword_text about_page collective collective_management_explained collective_manager_group_explained commonscloud_page edit group of managers index_page is better is not a good password maintenance_page min. 4 chars register_page service service_management_explained service_manager_group_explained support_page Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2023-12-01 23:46+0000
PO-Revision-Date: 2018-11-27 23:22+0100
Last-Translator: 
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
  If you don't receive it, check your Spam folder! %s removed OK 404 Page not found Sobre nosotros Activa la cuenta Active Add a new member Add group Add member Add or remove people from these collectives Add or remove people from these groups Add user Added %s Already exist in the database Already got an account and An account with this email already exists. Attribute '%s' not found Avatar Se cambió el avatar Cuerpo Cancela No se puede borrar. El usuario ya se exportó a LDAP Cannot remove user from Primary collective Cambia contraseña Colectivo Collective members Collectives Collectives you manage CommonsCloud Confirm your email at Commons Cloud Create group Create new group Created Hacen falta credenciales DN not valid Borrar Borra la entrada Descripción Does not exist Edita Edita avatar Edit profile Edit your profile Correo-e Dirección del correo-e Correo confirmado OK Correo enviado Email sent OK No coinciden los correos Error connecting to Mailtrain. More info on stderr logs.  Failed to create %s Finish onboarding process at Commons Cloud ¿Te has olvidado la contraseña? De Nombre completo Grant or deny people access to these services Group Group created Group management Group members Grupos Groups of people Hola I accept the <a href='%s' target='_blank'>conditions of use</a> and <a href='%s' target='_blank'>privacy policy.</a> I want to subscribe to the Commonscloud newsletter. If the user is not found, send this link via email Formatos de imagen Insufficient permission: %s Idioma Entrar Salir Looking for help? Maintenance mode Manage groups for: Managers Miembros Mi perfil Nombre Contraseña nueva New user requesting an account No file part No selected file Notify manager Onboarding terminado No se ha terminado el onboarding todavía Contraseña cambiado OK Contraseñas deben coincidir Please edit Please login Please set your new password Preferred language Quota Destinatarias Recuperar la contraseña Recover password at Commons Cloud Register Remove user Remove user from group Repeat email address Repeat password Return Guardar Buscar Search for the person Enviar correo Envía el correo de nuevo Enviar correo a Send email to members Envía un correo al usuario para fijar la contraseña Sent activation email Sent email to manager(s) Servicio Services Services you manage Fija la contraseña Short biography Apúntate State Asunto Subscribe to newsletter Soporte Apellido Dinos algo de ti Thank you for confirming your email address. We'll send you another email to configure your password The new username will be The onboarding process for CommonsCloud goes as follows. The clock icon tells you where you are! Hace dos veranos había pulgas en el hotel Menorquina. Piensa en una contraseña más larga y fácil de recordar Eso no se puede deshacer This element already exists. No se puede actualizar Unattended Actualizado correctamente User Se ha borrado el usuario User is already in the LDAP DIT No se encuentra el usuario Usuario no encontrado: %s Nombre de usuario Username is already taken Users Users you manage Valid credentials needed Visit our community support! Waiting for user to set password We sent you the first of two emails. In this one we ask you to comfirm your email address Te hemos enviado un correo Website Sitio web Bienvenido You can manage these groups You can manage your user profile here You can now access your CommonsCloud tools You may login here You will be sent a copy Your account is created Tus grupos Your new username Your password has been changed successfully Your username Tu nombre de usuario es Tus sitios web Bienvenido a commonsCloud.

Por favor, confirma tu dirección de correo en este enlace
%s

Cordiales saludos. Bienvenido a commonsCloud.

Por favor, confirma tu dirección de correo en este enlace
%s

Cordiales saludos. Hola,

Una persona está esperando que le des de alta en %s.

Nombre: %s
email: %s

%s

Kind regards. Hola %s,

Alguien a pedido que se cambie tu contraseña.
Si no ha sido tu, por favor ignora este correo.

Tu nombre de usuario es: %s

Puedes cambiar tu contraseña del commonsCloud aquí
%s

Cordiales saludos. Hola %s,

Se ha creado tu cuenta en commonsCloud.
Tu nombre de usuario es: %s (Recuérdalo!)

Por favor, establece tu contraseña aquí
%s

Cordiales saludos. <p>CommonsCloud.coop está formado por personas y entidades que mancomunan herramientas de nube digital en un proyecto tecnoético y cooperativo con software libre; así como aquellas que se hacen cargo del servicio, de la implementación, el mantenimiento, la gestión y el soporte. Es impulsado por la cooperativa integral femProcomuns SCCL en intercooperación con otras cooperativas y entidades. Más información: <a href="https://www.commonscloud.coop/es/quienes-somos/">https://www.commonscloud.coop/es/quienes-somos</a></p> colectivo All Commons Cloud users are grouped together in 'Collectives'.
<br />
You manage members of the collective and can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. Members of this group are <b>Collective Managers</b> and will <b>receive an email notification</b> when an anonymous person requests an account as a collective member.
<br />
Managers can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses.  <p><strong><a href="https://www.commonscloud.coop/es/">CommonsCloud.coop</a> es un servicio de nube <a href="https://www.commonscloud.coop/es/cooperativo/">cooperativa</a>, <a href="https://www.commonscloud.coop/es/tecnoetico/">tecnoética</a> y hecha con <a href="https://www.commonscloud.coop/es/software-libre/">software libre</a>, que integra diferentes herramientas digitales para trabajar de forma ágil y eficiente. </strong></p><p>Para <strong>personas individuales</strong> ofrece <a href="https://www.commonscloud.coop/es/servicios/">servicios</a> de comunicación, de almacenaje en la nube y de teletrabajo, como la oficina y el servicio de correo, entre otros.</p><p>Para <strong>organizaciones</strong> ofrece <a href="https://www.commonscloud.coop/es/servicios/">servicios</a> para la comunicación, la colaboración, el teletrabajo ágil en equipo, la gestión económica y de proyectos, como la oficina, el correo, la videoconferencia, el ágora y el gestor ERP.</p> edita group of managers <p>Crea una cuenta para acceder a los servicios de CommonsCloud:</p><p><a href="https://gent.commonscloud.coop/registre">https://gent.commonscloud.coop/registre</a></p> es mejor no es una buena contraseña maintenance_page min. 4 chars <p>
Un sol compte per accedir a tots els serveis de CommonsCloud; Oficina, Agora i Projectes.
<br />
Alguns serveis són oberts i gratuïts, per altres caldrà que et facis sòcia cooperativista. <a href="/about">Informa't</a>
</p> servicio Services normally refer to websites, however a service is in fact any Commons Cloud service that requires a user to login.
<br />
You can add or remove <b>any user with a Commons Cloud account</b> to the a service you manage, meaning <b>you decide who can login or not</b>. Members of this group are <b>Service Managers</b> and can add or remove any user with a Commons Cloud account, meaning they <b>decide who can login or not</b>. <h4>Como encontrar ayudar</h4>
<ol>

    <li>Lee la documentación</li>

    <li>Consulta con la comunidad de usuarios</li>

    <li>Abrir una incedencia</li>

</ol>

<table>


    <tr>

    <td><b>Oficina</b></td>

    <td><a target="_blank" href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/oficina/">Documentación</a> <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-loficina">Comunidad</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidencias</td>

    </tr>


    <tr>

    <td><b>Projectes</b></td>

    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/projectes/">Documentación</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-projectes">Comunidad</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidencias</td>

    </tr>


    <tr>

    <td><b>Àgora</b></td>

    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/agora/">Documentación</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/site-feedback">Comunidad</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidencias</td>

    </tr>

</table>

<h4>CommonsCloud</h4>
<p>
Comparte dudas y soluciones sobre la plataforma CommonsCloud
<br />
<a target="_blank"  href="https://agora.commonscloud.coop/c/commonscloud">https://agora.commonscloud.coop/c/commonscloud</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
</p>

<h4>No puedo hacer un login</h4>
<p>
Tu cuenta te usuario serve para logear en todos los servicios que tienes disponibles de CommonsCloud.
<br />
No hace falta tener más de una cuenta.
<br />
<br />
Si no puedes logear, <a href="https://www.commonscloud.coop/recover-password">resetea tu contraseña</a>
<br />
Te presente que tu "nombre de usuario", No es tu correo electrónico.
<br />
Si aun así no puedes hacer un login, escríbenos a suport@commonscloud.coop
<p/> 