��    �      �              \     ]     k     ~     �     �     �  	   �  
   �  +   �  &   �          !     *     H  *   c     �     �     �     �     �  -   �  *   �     %  
   5     @     S     _     v  #   �     �     �     �     �     �     �     �                    !     -     :     L     R     `  
   s     ~     �  9   �     �  *   �          2     7  -   @     n     t     �     �     �     �     �  t   �  3   4  2   h     �     �     �     �     �     �     �     �            
   "     -     2     ?     ^     k     |     �     �     �     �     �     �            
   $     /  !   @     b     k     w     �     �     �     �     �     �  
   �     �     �       &        C     Y     r     z     �     �     �     �     �     �     �     �      �  d   �     `  (   y  0   �     �     �       
     
   !     ,     1     >     ^     m     �     �     �     �     �     �      �          )     1     :     B  %   ^  *   �     �     �     �     �     �  +        <     J     [     i     �     �     �     �  
   �  
   �     �  "        6     H     M  
   _  	   j     t     �     �     �     �     �     �     �  e  	     o     �     �     �     �     �     �     �  0   �  )   )     S  	   a     k     �  4   �     �     �     �            3   #  9   W     �     �     �     �     �  	   �  2   �     0     =     N     U     h     t     }     �     �     �     �     �     �     �     �  (        5     C  (   ]  9   �     �  .   �          !     $  -   2     `  
   e     p     �     �     �     �  |   �  -   ,   2   Z      �      �      �      �      �      �      �      �      !     !     !     #!     '!     8!     W!     j!     �!  !   �!  !   �!  !   �!     �!     "  )   "     G"     W"     ]"     j"  %   �"     �"     �"     �"     �"     �"     #     #     #     "#     3#     C#     \#  !   n#  5   �#     �#  ,   �#     $     $     $     2$     F$     U$     d$     j$     s$     z$     �$  e   �$     %  2    %  9   S%     �%     �%     �%  	   �%     �%     &     	&      &     :&     K&     `&     m&     �&     �&     �&  '   �&  7   �&  "   &'     I'     R'     ['     h'  -   �'  (   �'     �'     �'     (     !(     0(  -   D(     r(     �(     �(  m   �(  �   )  W  �)  �   +  ~  �+  �  a-     W/  �   c/  9  T0  o  �1     �3     4  �   4  
   �4     �4     �4     5  -  5     G6    N6  �   `7  �   8   %s removed OK 404 Page not found About us Activate account Active Add a new member Add group Add member Add or remove people from these collectives Add or remove people from these groups Add user Added %s Already exist in the database Already got an account and An account with this email already exists. Attribute '%s' not found Avatar Avatar changed OK Body Cancel Cannot delete. User has been exported to LDAP Cannot remove user from Primary collective Change password Collective Collective members Collectives Collectives you manage CommonsCloud Confirm your email at Commons Cloud Create group Create new group Created Credentials needed DN not valid Delete Delete entry Description Does not exist Edit Edit avatar Edit profile Edit your profile Email Email address Email confirmed OK Email sent Email sent OK Emails do not match Error connecting to Mailtrain. More info on stderr logs.  Failed to create %s Finish onboarding process at Commons Cloud Forgotten your password? From Fullname Grant or deny people access to these services Group Group created Group management Group members Groups Groups of people Hello I accept the <a href='%s' target='_blank'>conditions of use</a> and <a href='%s' target='_blank'>privacy policy.</a> I want to subscribe to the Commonscloud newsletter. If the user is not found, send this link via email Image formats Insufficient permission: %s Language Login Logout Looking for help? Maintenance mode Manage groups for: Managers Members My profile Name New password New user requesting an account No file part No selected file Onboarding complete Onboarding not yet finished Password changed OK Passwords must match Please edit Please login Please set your new password Preferred language Quota Recipients Recover password Recover password at Commons Cloud Register Remove user Remove user from group Repeat email address Repeat password Return Save Search Search for the person Send email Send email again Send email to Send email to members Send the user an email to set password Sent activation email Sent email to manager(s) Service Services Services you manage Set password Short biography Sign up State Subject Support Surname Tell us something about yourself Thank you for confirming your email address. We'll send you another email to configure your password The new username will be There were fleas last summer in Menorca. Think of a longer, easier to remember passphrase This can't be undone This element already exists. Unable to update Unattended Updated OK User User deleted User is already in the LDAP DIT User not found User not found: %s Username Username is already taken Users Users you manage Valid credentials needed Visit our community support! Waiting for user to set password We've sent you an email Website Websites Welcome You can manage these groups You can manage your user profile here You can now access your CommonsCloud tools You may login here You will be sent a copy Your account is created Your groups Your new username Your password has been changed successfully Your username Your username is Your websites _confirmEmailAddress_email_text _confirmEmail_text _newUserWaiting_email_text _resetPassword_text _setPassword_text about_page collective collective_management_explained collective_manager_group_explained commonscloud_page edit group of managers index_page is better is not a good password maintenance_page min. 4 chars register_page service service_management_explained service_manager_group_explained support_page Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2023-12-01 23:46+0000
PO-Revision-Date: 2018-11-27 23:22+0100
Last-Translator: 
Language-Team: ca <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
 %s correctament eliminat 404 pàgina no trobada Sobre nosaltres Activar compte Actiu Afegeix un nou membre Afegeix un grup Afegir sòcia Afegeix o elimina membres d'aquests col·lectius Afegeix o elimina membres d'aquests grups Afegir usuari Afegit %s Ja existeix en la base de dades Ja tens un compte d'usuari i Ja existeix un compte amb aquesta adreça de correu. Atribut '%s' no trobat Avatar Canvi d'avatar correcte Cos Cancel·lar No es pot eliminar. Uusari ha sigut exportat a LDAP No és possible eliminar l'usuari del Col·lectiu Primari Canvia la contrasenya Col·lectiu Membres del col·lectiu Col·lectius Col·lectius que gestiones SomNúvol Confirma el teu correu electrònic a Commons Cloud Crea un grup Crea un nou grup Creats Falten credencials DN invàlid Eliminar Eliminar entrada Descripció No existeix Edita Edita avatar Editar perfil Edita el teu perfil Correu electrònic Adreça de correu electrònic Confirmació correu electrònic correcte Correu enviat Email enviat correctament Els correus electrònics no coincideixen Error connecting to Mailtrain. More info on stderr logs.  No s'ha pogut crear %s Acaba el procés d'embarcament a Commons Cloud Has perdut la contrasenya? De Nom i cognoms Concedeix o denega l'accés a aquests serveis Grup Grup creat Gestió del grup Membres del grup Grups Grups de persones Hola Accepto les <a href='%s' target='_blank'>condicions d'ús</a> i la <a href='%s' target='_blank'>política de privacitat</a>. Em vull subscriure al butlletí de SomNúvol. If the user is not found, send this link via email Formats d'imatge Permisos insuficients: %s Idioma Entra Surt Necessites ajuda? Mode de manteniment Gestiona grups per: Gestores Sòcies El meu perfil Nom Contrasenya nova Un usuari nou demana un compte Cap part de fitxer Cap fitxer seleccionat Embarcament acabat L'embarcament encara no ha acabat Contrasenya canviada correctament Les contrasenyes han de coincidir Si us plau edita Si us plau, entra Si us plau, estableix la nova contrasenya Idioma preferit Quota Destinataris Recupera la contrasenya Recuperar contrasenya a Commons Cloud Registre Esborrar usuari Elimina l'usuari del grup Repeteix l'adreça de correu Repeteix contrasenya Tornar Guarda Cercar Busca la persona Envia el correu Torna a enviar el correu Envia un correu a Envia un email a tots els membres Envia a l'usuari un correu per definir la contrasenya Email d'activació enviat Correu enviat a les gestores del col·lectiu Servei Serveis Serveis que gestiones Posa la contrasenya Breu biografia Crea un compte Estat Assumpte Suport Cognoms Explica'ns alguna cosa sobre tu Gràcies per confirmar la teva adreça. T'enviarem un altre correu per configurar la teva contrasenya El nou nom d'usuari és Fa dos estius hi havia puces a l'hotel Menorquina. Pensa en una contrasenya més llarga i fàcil de recordar Això no es pot desfer Aquest element ja existeix. Actualització no és possible No atesos Actualitzat correctament Usuari Usuari eliminat L'usuari ja existeix al DIT LDAP Usuari no trobat Usuari no trobat: %s Nom d'usuari Aquest nom d'usuari ja existeix Usuaris Usuaris que gestiones Falten credencials vàlids Visita la nostra comunitat d'usuàries! Esperant que la persona usuària canviï la contrasenya T'hem enviat un correu electrònic Lloc web Pàgines Benvingut/da Pots gestionar aquests grups Pots gestionar el teu perfil d'usuària aquí Ja pots accedir a les eines de SomNúvol Pots entrar aquí Se t'enviarà una còpia El teu compte està creat Els teus grups El teu nom d'usuari La teva contrasenya s'ha canviat correctament El teu nom d'usuari El teu nom d'usuari és Les teves pàgines Benvingut/da a commonsCloud.

Si us plau, confirma la teva adreça de correu en aquest enllaç
%s

Fins aviat Hola!

Estem creant el teu compte a SomNuvol.coop.
Et demanem que confirmis la teva adreça en aquest enllaç:
%s

Gràcies,
Equip SomNúvol Hola,

Hi ha una persona que ha demanat un compte per a %s.

Nom: %s
Correu electrònic: %s

El següent enllaç et portarà a la pàgina de validació de la petició. Cal que (1)  revisis la petició,  (2) activis els serveis que ha demanat i (3) enviis el correu a la persona usuària perquè pugui canviar la seva contrasenya.%s

Fins aviat Hola %s,

Una persona ha demanat canviar la seva contrasenya.
Si no l'has demanat tu, si us plau ignora aquest correu.

El teu nom d'usuari és: %s

Pots canviar tu la teva contrasenya de commonsCloud aquí
%s

Fins aviat Hola %s,

Ja hem creat el teu compte d'usuari a SomNúvol.
El teu nom d'usuari és: %s (Recorda'l!)

Si us plau, estableix la teva contrasenya clicant aquest enllaç:
%s

Pots fer servir aquest nom d'usuari i contrasenya per accedir a l'Àgora, un espai compartit amb la resta de persones i entitats sòcies de SomNúvol:
https://agora.commonscloud.coop

Fins aviat!
Equip SomNúvol <p>
CommonsCloud.coop el formen les persones i entitats que mancomunen eines de núvol digital en un projecte tecnoètic i cooperatiu amb programari lliure; així com aquelles que es fan càrrec del servei, de la implementació, el manteniment, la gestió i el suport. És impulsat per la cooperativa integral femProcomuns SCCL en intercooperació amb altres cooperatives i entitats. Per més informació, veieu <a href="https://www.somnuvol.coop/qui-som/">https://www.somnuvol.coop/qui-som/</a>
</p>
 col·lectiu All Commons Cloud users are grouped together in 'Collectives'.
<br />
You manage members of the collective and can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. Members of this group are <b>Collective Managers</b> and will <b>receive an email notification</b> when an anonymous person requests an account as a collective member.
<br />
Managers can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. <p><strong><a href="https://www.somnuvol.coop/">SomNuvol.coop</a> és un servei de núvol <a href="https://www.commonscloud.coop/cooperatiu/">cooperatiu</a>, <a href="https://www.commonscloud.coop/tecnoetic/">tecnoètic</a> i fet amb <a href="https://www.commonscloud.coop/programari-lliure-2/">programari lliure</a>, que integra diferents eines digitals per treballar de manera àgil i eficient.</strong></p><p>Per a <strong>persones individuals i organitzacions</strong> ofereix <a href="https://www.commonscloud.coop/serveis/">eines</a> d'oficina virtual, correu electrònic, videoconferència, ERP/CRM i Formularis.</p> editar grup de gestores <p>
<strong>Si encara no en tens un, crea un compte a SomNúvol:</strong>
</p>
<p>
<a href="https://gent.commonscloud.coop/registre">https://gent.commonscloud.coop/registre</a>
</p>
 és millor no és una bona contrasenya maintenance_page mínim 4 caràcters <p>
Un sol compte per accedir a totes les eines de SomNúvol: Oficina, Correu, Videoconferència, Gestor ERP/CRM, Formularis i Àgora.
<br />
Alguns serveis són oberts i gratuïts, per altres caldrà que et facis sòcia cooperativista. <a href="https://www.somnuvol.coop/eines/">Informa-te'n</a>
</p> servei Services normally refer to websites, however a service is in fact any Commons Cloud service that requires a user to login.
<br />
You can add or remove <b>any user with a Commons Cloud account</b> to the a service you manage, meaning <b>you decide who can login or not</b>. Members of this group are <b>Service Managers</b> and can add or remove any user with a Commons Cloud account, meaning they <b>decide who can login or not</b>. <h4>Com trobar ajuda</h4>
<ol>

    <li>Llegeix la documentació ;)</li>

    <li>Consulta con la comunitat d'usuaris</li>

    <li>Obre una incidència</li>

</ol>

<table>


    <tr>

    <td><b>Oficina</b></td>

    <td><a target="_blank" href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/oficina/">Documentación</a> <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-loficina">Comunitat</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidències</td>

    </tr>


    <tr>

    <td><b>Projectes</b></td>

    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/projectes/">Documentació</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-projectes">Comunitat</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidències</td>

    </tr>


    <tr>

    <td><b>Àgora</b></td>

    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/agora/">Documentació</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>


    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/site-feedback">Comunitat</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidències</td>

    </tr>

</table>

<h4>Comunidad</h4>
<p>
Comparteix dubtes i solucions sobre la plataforma CommonsCloud
<br />
<a target="_blank"  href="https://agora.commonscloud.coop/c/commonscloud">https://agora.commonscloud.coop/c/commonscloud</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
</p>

<h4>No puc entrar-hi</h4>
<p>
El teu compte d'usuària serveix per entrar a tots els serveis que tens a la teva disponibilitat a CommonsCloud.
<br />
Només cal un compte d'usuària.
<br />
<br />
Si no pots entrar-hi, <a href="https://www.commonscloud.coop/recover-password">demana una nova contrasenya</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
<br />
Recorda que el teu "nom d'usuària" no es el teu correu electrònic!
<br />
Si encara no has pogut entrar, envia'ns un coreu a suport@commonscloud.coop
<p/> 