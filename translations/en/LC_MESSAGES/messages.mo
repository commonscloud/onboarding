��    �                      1        ?     M     `     i     z     �  	   �  
   �  +   �  &   �     �               *  *   E     p     �     �     �     �  -   �  *   �       
        "     5     A     X  #   e     �     �     �     �     �     �     �     �     �     �                    ,     >     D     R  
   e     p     ~  9   �     �  *   �          $     )  -   2     `     f     t     �     �     �     �  3   �  2   �          &     B     O     X     ^     e     w     �     �     �  
   �     �     �     �     �     �               )     E     Y     n     w     �     �     �     �     �  
   �     �  !   �                ,     C     X     h     o     t     {  
   �     �     �     �  &   �     �          '     /     8     L     Y     i     q     w          �     �      �  d   �     -  `   F  (   �  0   �               3  
   D  
   O     Z     _     l     �     �     �     �     �     �     �     �           ,  Y   M     �     �     �     �     �  %   �  *        E     X  -   p     �     �     �  +   �                     -     M     `     {     �  
   �  
   �     �  "   �     �                 
   )  	   4     >     U     f     s     �     �     �     �  s  �  1   G     y     �     �     �     �     �  	   �  
   �  +   �  &        4     =     F     d  *        �     �     �     �     �  -   �  *        A  
   Q     \     o     {     �  #   �     �     �     �     �     �     	               -     9     H     M     Y     f     x     ~     �  
   �     �     �  9   �        *         E      ^      c   -   l      �      �      �      �      �      �      �   3   �   2   !     R!     `!     |!     �!     �!     �!     �!     �!     �!     �!     �!  
   �!     �!     �!     "     ""     /"     @"     O"     c"     "     �"     �"     �"     �"     �"     �"     �"     #  
   #     #  !   /#     Q#     Z#     f#     }#     �#     �#     �#     �#     �#  
   �#     �#     �#     �#  &   $     2$     H$     a$     i$     r$     �$     �$     �$     �$     �$     �$     �$     �$      �$  d   %     g%  `   �%  (   �%  0   
&     ;&     P&     m&  
   ~&  
   �&     �&     �&     �&     �&     �&     �&     �&     �&     '     '     0'     I'      f'  Y   �'     �'     �'     (     
(     (  %   .(  *   T(     (     �(  -   �(     �(     �(     �(  +   )     :)     H)     Y)  R   g)  n   �)  \   )*  �   �*  �   K+    �+  
   �-  �   �-  9  �.  ;  (0     d3     i3     o3  �   �3  	   '4     14     H4     Y4  �   f4     @5    H5  �   Z6  f  �6    If you don't receive it, check your Spam folder! %s removed OK 404 Page not found About us Activate account Active Add a new member Add group Add member Add or remove people from these collectives Add or remove people from these groups Add user Added %s Already exist in the database Already got an account and An account with this email already exists. Attribute '%s' not found Avatar Avatar changed OK Body Cancel Cannot delete. User has been exported to LDAP Cannot remove user from Primary collective Change password Collective Collective members Collectives Collectives you manage CommonsCloud Confirm your email at Commons Cloud Create group Create new group Created Credentials needed DN not valid Delete Delete entry Delete petition Description Does not exist Edit Edit avatar Edit profile Edit your profile Email Email address Email confirmed OK Email sent Email sent OK Emails do not match Error connecting to Mailtrain. More info on stderr logs.  Failed to create %s Finish onboarding process at Commons Cloud Forgotten your password? From Fullname Grant or deny people access to these services Group Group created Group management Group members Groups Groups of people Hello I want to subscribe to the Commonscloud newsletter. If the user is not found, send this link via email Image formats Insufficient permission: %s Kind regards Language Login Logout Looking for help? Maintenance mode Manage groups for: Managers Members My profile Name New password New user requesting an account No file part No selected file Notify manager Onboarding complete Onboarding not yet finished Password changed OK Passwords must match Petition Please edit Please login Please set your new password Preferred language Primary collective Quota Recipients Recover password Recover password at Commons Cloud Register Remove user Remove user from group Repeat email address Repeat password Return Save Search Search for the person Send email Send email again Send email to Send email to members Send the user an email to set password Sent activation email Sent email to manager(s) Service Services Services you manage Set password Short biography Sign up State Subject Subscribe to newsletter Support Surname Tell us something about yourself Thank you for confirming your email address. We'll send you another email to configure your password The new username will be The onboarding process for CommonsCloud goes as follows. The clock icon tells you where you are! There were fleas last summer in Menorca. Think of a longer, easier to remember passphrase This can't be undone This element already exists. Unable to update Unattended Updated OK User User deleted User is already in the LDAP DIT User not found User not found: %s User petition Username Username is already taken Users Users you manage Valid credentials needed Visit our community support! Waiting for user to set password We sent you the first of two emails. In this one we ask you to comfirm your email address We've sent you an email Website Websites Welcome You can manage these groups You can manage your user profile here You can now access your CommonsCloud tools You may login here You will be sent a copy You'll receive an e-mail to set your password Your account is created Your groups Your new username Your password has been changed successfully Your username Your username is Your websites _confirmEmailAddress_email_text _confirmEmail_text _newUserWaiting_email_text _resetPassword_text _setPassword_text about_page collective collective_management_explained collective_manager_group_explained commonscloud_page edit email group of managers index_page is better is not a good password maintenance_page min. 4 chars register_page service service_management_explained service_manager_group_explained support_page Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2023-12-01 23:46+0000
PO-Revision-Date: 2018-11-27 23:22+0100
Last-Translator: 
Language-Team: en <chris@commonscloud.coop>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
  If you don't receive it, check your Spam folder! %s removed OK 404 Page not found About us Activate account Active Add a new member Add group Add member Add or remove people from these collectives Add or remove people from these groups Add user Added %s Already exist in the database Already got an account and An account with this email already exists. Attribute '%s' not found Avatar Avatar changed OK Body Cancel Cannot delete. User has been exported to LDAP Cannot remove user from Primary collective Change password Collective Collective members Collectives Collectives you manage CommonsCloud Confirm your email at Commons Cloud Create group Create new group Created Credentials needed DN not valid Delete Delete entry Delete petition Description Does not exist Edit Edit avatar Edit profile Edit your profile Email Email address Email confirmed OK Email sent Email sent OK Emails do not match Error connecting to Mailtrain. More info on stderr logs.  Failed to create %s Finish onboarding process at Commons Cloud Forgotten your password? From Fullname Grant or deny people access to these services Group Group created Group management Group members Groups Groups of people Hello I want to subscribe to the Commonscloud newsletter. If the user is not found, send this link via email Image formats Insufficient permission: %s Kind regards Language Login Logout Looking for help? Maintenance mode Manage groups for: Managers Members My profile Name New password New user requesting an account No file part No selected file Notify manager Onboarding complete Onboarding not yet finished Password changed OK Passwords must match Petition Please edit Please login Please set your new password Preferred language Primary collective Quota Recipients Recover password Recover password at Commons Cloud Register Remove user Remove user from group Repeat email address Repeat password Return Save Search Search for the person Send email Send email again Send email to Send email to members Send the user an email to set password Sent activation email Sent email to manager(s) Service Services Services you manage Set password Short biography Sign up State Subject Subscribe to newsletter Support Surname Tell us something about yourself Thank you for confirming your email address. We'll send you another email to configure your password The new username will be The onboarding process for CommonsCloud goes as follows. The clock icon tells you where you are! There were fleas last summer in Menorca. Think of a longer, easier to remember passphrase This can't be undone This element already exists. Unable to update Unattended Updated OK User User deleted User is already in the LDAP DIT User not found User not found: %s User petition Username Username is already taken Users Users you manage Valid credentials needed Visit our community support! Waiting for user to set password We sent you the first of two emails. In this one we ask you to comfirm your email address We've sent you an email Website Websites Welcome You can manage these groups You can manage your user profile here You can now access your CommonsCloud tools You may login here You will be sent a copy You'll receive an e-mail to set your password Your account is created Your groups Your new username Your password has been changed successfully Your username Your username is Your websites Welcome to commonsCloud.

Please confirm your email at this link
%s

Kind regards. Welcome to CommonsCloud!

Please confirm your email clicking on this link:
%s

Kind regards,
CommonsCloud Team Hello,

There is person requesting an account for %s.

Name: %s
email: %s

%s

Kind regards. Hello %s,

Someone has requested your password to be changed.
If you did not do it, please ignore this email.

Your username is: %s

Please set your password at femProcomuns here
%s

Kind regards. Hello %s,

Your account has been created.
Your username is: %s (Remember this!)

Please set your password at femProcomuns here
%s

Kind regards. <p>
CommonsCloud.coop is made up of people and organisations that pool digital cloud resources in a technological and cooperative project with free software; people and organisations responsible for the implementation, maintenance, management and support of the service. It is promoted by femProcomuns SCCL multi-stakeholder cooperative in intercooperation with other cooperatives and organisations. For more information, see <a href="https://www.commonscloud.coop/en/who-are-we/">https://www.commonscloud.coop/en/who-are-we/</a></p> collective All Commons Cloud users are grouped together in 'Collectives'.
<br />
You manage members of the collective and can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. Members of this group are <b>Collective Managers</b> and will <b>receive an email notification</b> when an anonymous person requests an account as a collective member.
<br />
Managers can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. <p><strong><a href="https://www.commonscloud.coop/en/">CommonsCloud.coop</a> is a  <a href="https://www.commonscloud.coop/en/cooperative/">cooperative</a> and <a href="https://www.commonscloud.coop/en/en/ethical-tech/">ethical-tech</a> cloud service, made with <a href="https://www.commonscloud.coop/en/free-software/">free software</a>. It integrates different digital tools to work in an agile and efficient way.</strong></p><p><strong>For individuals</strong> we offer communication, storage and remote working <a href="https://www.commonscloud.coop/en/services/">services</a>, such as an online office and an email service.</p><p><strong>For organisations</strong> we offer communication, collaboration, agile remote teamwork, finance and project managementsuch as an online office, videoconferencing, email and an ERP.</p> edit email group of managers <p>
Create an account to access CommonsCloud services:</p>
<p>
<a href="https://gent.commonscloud.coop/registre">https://gent.commonscloud.coop/registre</a></p></p>
 is better is not a good password maintenance_page min. 4 chars <p>
Just one account to access all CommonsCloud services; Office, Agora i Projects.
<br />
Some services are open and free, others require you to become part of our cooperative. <a href="/about">Find out more</a>
</p> service Services normally refer to websites, however a service is in fact any Commons Cloud service that requires a user to login.
<br />
You can add or remove <b>any user with a Commons Cloud account</b> to the a service you manage, meaning <b>you decide who can login or not</b>. Members of this group are <b>Service Managers</b> and can add or remove any user with a Commons Cloud account, meaning they <b>decide who can login or not</b>. <h4>How to find help</h4>
<ol>
    <li>Read the documentation ;)</li>
    <li>Ask the community of users</li>
    <li>Open an issue</li>
</ol>

<table>
    <tr>
    <td><b>Office</b></td>
    <td><a target="_blank" href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/oficina/">Documentation</a> <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-loficina">Community</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td>Issues</td>
    </tr>
    <tr>
    <td><b>Projectes</b></td>
    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/projectes/">Documentation</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-projectes">Community</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td>Issues</td>
    </tr>
    <tr>
    <td><b>Àgora</b></td>
    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/agora/">Documentation</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/site-feedback">Community</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td>Issues</td>
    </tr>
</table>

<h4>CommonsCloud</h4>
<p>
Share doubts, suggestions and solutions about the CommonsCloud platform
<br />
<a target="_blank"  href="https://agora.commonscloud.coop/c/commonscloud">https://agora.commonscloud.coop/c/commonscloud</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
</p>

<h4>I can't login!</h4>
<p>
Your same user account is valid for all the services you have access to on the platform.
<br />
You only need one user account
<br />
<br />
If you can't login, <a href="https://www.commonscloud.coop/recover-password">reset your password</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
<br />
Remember that your username is not your email!
<br />
If you still can't login, send us an email at suport@commonscloud.coop
<p/> 