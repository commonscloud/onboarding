# Add new users to the DIT

"Onboarding" is a LDAP web client where people can request an account on the DIT.
It also includes a couple of static web pages.

## Install

First preprare a virtual environment, python libraries (via pip), gunicorn and supervisor installation. [[ https://phabricator.femprocomuns.cat/w/commonscloud/ldap/admin/ | See here ]].

```
cd /opt/ldap
source ./venv/bin/activate
pip install unidecode, flask_sqlalchemy, flask_babel, flask_paginate
git clone https://gitlab.com/femprocomuns/onboarding.git
```
//(we need to set up requirement.txt properly)//


Edit `/opt/ldap/manage_onboarding.py`

```
from flask_script import Manager, Server
from onboarding import app

manager = Manager(app)

# Turn on debugger by default and reloader
manager.add_command("run", Server(
    use_debugger = True,
    use_reloader = True,
    threaded = True,
    port = '5002',
    host = '0.0.0.0')
)

if __name__ == "__main__":
    manager.run()
```

Create and edit `/opt/ldap/onboarding/config.cfg`
```
cp /opt/ldap/onboarding/config.cfg.example /opt/ldap/onboarding/config.cfg
```


Edit `/opt/ldap/gunicorn-onboarding.conf`

```
command = '/opt/ldap/venv/bin/gunicorn'
pythonpath = '/opt/ldap'
bind = '0.0.0.0:5002'
workers = 3
user = 'www-data'
```

Edit `/etc/supervisor/conf.d/onboarding.conf`

```
[program:onboarding]
command = /opt/ldap/venv/bin/gunicorn -c /opt/ldap/gunicorn-onboarding.conf onboarding:app
directory = /opt/ldap
user = www-data
```

# How to update translation files

 - Install python-babel package if needed
```
apt-get install python-babel
```
 - Updating locales
```
pybabel extract -F babel.cfg -o translations/messages.pot --input-dirs=.
```
 - Run pybabel update to update your locale file messages.po
```
pybabel update -i translations/messages.pot -d translations
```
 - Translate the new strings and compile your locale:
```
pybabel compile -d translations
```
