# -*- coding: utf-8 -*-
from onboarding import app, db
from wtforms import ValidationError, BooleanField, StringField, TextAreaField, DateField, PasswordField, SelectField, validators
from flask_wtf import FlaskForm
from flask_babel import lazy_gettext as _
from flask_babel import gettext
import datetime, re, ldap
from unidecode import unidecode
from sqlalchemy import and_

# http://flask-sqlalchemy.pocoo.org/2.3/quickstart/

from sqlalchemy import Boolean
from sqlalchemy import TypeDecorator

class LiberalBoolean(TypeDecorator):
    impl = Boolean

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = value.data
            #value = bool(int(value))
        return value

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    surname = db.Column(db.String(120))
    mail = db.Column(db.String(120))
    language = db.Column(db.String(2), nullable=False, default="en")
    validated_mail = db.Column(db.Boolean, default=False, nullable=False) # user replied to an email we sent them
    token = db.Column(db.String(120), unique=True)
    short_bio = db.Column(db.Text())
    primary_collective = db.Column(db.String(100))
    created = db.Column(db.Date) # petition for an account made by user
    exported = db.Column(db.Date, default=None) # has been revised by Manager and ldap account created
    activation_email_sent = db.Column(db.Date, default=None)
    activated = db.Column(db.Boolean, default=False, nullable=False) # user has set password
    newslettersub = db.Column(LiberalBoolean, default=False, nullable=False) # user has set password
    uid = db.Column(db.String(35), unique=True)
    dn = db.Column(db.String()) # LDAP distinguished name

    def __init__(self, name, surname, language, short_bio, mail, uid, primary_collective, newslettersub):
        self.name = name
        self.surname = surname
        self.language = language
        self.short_bio = short_bio
        self.mail = mail
        self.uid = uid
        self.primary_collective = primary_collective
        self.created = datetime.datetime.now()
        self.newslettersub = newslettersub

    def getClassName(self):
            return self.__class__.__name__

    @property
    def primary_collective_dn(self):
        return "cn=%s,%s" % (self.primary_collective, app.config['COLLECTIVE_OU_DN'])
        
    def __repr__(self):
        return '<User %s %s>' % (self.name, self.surname)
    
    def getLanguage(self):
        return app.config['LANGUAGES'][self.language].decode("utf-8")
        
    def isUsernameAvailable(self, uid):
        print uid
        return false
                   

class UniqueValidator(object):
    def __init__(self, model, field):
        self.model = model
        self.field = field
        self.message = _("This element already exists.")
    
    def __call__(self, form, field):
        if self.field.name == 'mail':
            self.message = _("An account with this email already exists.")
            # here we check for emails of accounts that are not yet ldap accounts.
            # unique ldap email attr is checked in the view
            check = self.model.query.filter(and_(self.field == field.data, self.model.activated == False)).first()
        else:
            check = self.model.query.filter(self.field == field.data).first()
        if check:
            raise ValidationError(self.message)


class UIDValidator(object):
    def __init__(self, message=None):
        self.message = _("Username is already taken")

    def __call__(self, form, field):
        uid = field.data
        uid = uid.replace(" ", "")
        if not uid:
            return False
        
        uid = unidecode(uid)
        uid = ''.join(uid.lower().split())
        uid = re.sub('[^A-Za-z0-9\-]+', '', uid) # this also serves for ldap escaping!
        field.data = uid
        if User.query.filter_by(uid=uid).first():
            raise ValidationError(self.message)
        
        con = ldap.initialize(app.config['LDAP_SERVER'])
        con.protocol_version = ldap.VERSION3
        con.simple_bind_s(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
        
        result = con.search_s(app.config['USER_OU_DN'], ldap.SCOPE_SUBTREE, '(&(objectClass=inetOrgPerson)(uid=%s))' % uid)
        con.unbind_s()
        if result:
            raise ValidationError(self.message)
            
        return True

# http://flask.pocoo.org/docs/0.12/patterns/wtforms/
class RegistrationForm(FlaskForm):
    name = StringField(_("Name"), [
        validators.Length(min=2, max=30),
        validators.InputRequired()
    ])
    surname = StringField(_("Surname"), [
        validators.Length(min=3, max=30),
        validators.InputRequired()
    ])
    language = SelectField(_('Preferred language'),
        choices=tuple([(k,v.decode("utf-8")) for k,v in app.config['LANGUAGES'].iteritems()])
    )
    short_bio = TextAreaField(_("Short biography"), [
        validators.InputRequired(message=_("Tell us something about yourself")),
    ], render_kw={"style": "width:100%"})
    uid = StringField(_("Your new username"), [
        validators.DataRequired(),
        validators.Length(min=2, max=30),
        UIDValidator()
    ])
    mail = StringField(_('Email address'), [
        validators.Email(),
        validators.DataRequired(),
        UniqueValidator(User, User.mail),
        validators.EqualTo('confirm_mail', message=_('Emails do not match'))
    ])
    confirm_mail = StringField(_('Repeat email address'))
    primary_collective = StringField(_('Collective'), [
        validators.Length(max=120)
    ])
    accept_conditions = BooleanField((_("I accept the <a href='%s' target='_blank'>conditions of use</a> and <a href='%s' target='_blank'>privacy policy.</a>" % (app.config['CONDITIONS_TERMS'], app.config['PRIVACY_POLICY']))), [
        validators.InputRequired()
    ])
    newslettersub = BooleanField((_("I want to subscribe to the Commonscloud newsletter.")), [
        validators.Optional()
    ])


class SetPasswordForm(FlaskForm):
    password = PasswordField(_('New password'), [
        validators.Length(min=12),
        validators.InputRequired(),
        validators.EqualTo('confirm_password', message=_('Passwords must match'))
    ])
    confirm_password = PasswordField(_('Repeat password'))

class RecoverPasswordForm(FlaskForm):
    mail = StringField(_('Email address'), [
        validators.Email(),
        validators.DataRequired()
    ])

class GroupForm(FlaskForm):
    rdn = StringField(_("Name"), [
        validators.InputRequired(),
    ])
    description = StringField(_("Description"), [
        validators.InputRequired()
    ])
    url = StringField(_("Website"))

class GroupEmailForm(FlaskForm):
    subject = StringField(_("Subject"), [
        validators.InputRequired(),
    ])
    body = StringField(_("Body"), [
        validators.InputRequired()
    ])
