# -*- coding: utf-8 -*-
from flask import Flask, render_template, redirect, url_for, request, session, flash
#from flask_sqlalchemy import pagination
from werkzeug.utils import secure_filename
from flask_paginate import Pagination, get_page_parameter
from flask_wtf.csrf import CSRFError
from onboarding import app, db, babel
from flask_babel import gettext
from base64 import b64encode
import collections, datetime, sha, os, io, random, string
import json
from functools import wraps
from randomavatar.randomavatar import Avatar
from PIL import Image
from models import User, RegistrationForm, SetPasswordForm, RecoverPasswordForm, GroupForm, GroupEmailForm
from ldap_helper import *
from utils import *
import requests
import sys



# login required decorator
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        #print kwargs
        if 'username' in session and 'password' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login'))
    return wrap


# admin required decorator
def admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'admin' in session and session['admin'] == True:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login'))
    return wrap

def valid_DN(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if not isValidDN(kwargs['dn']):
            flash(gettext("DN not valid"), 'error')
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return wrap    


# admin required decorator
def manager_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'managed_collectives' in session and session['managed_collectives']:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login'))
    return wrap

    
@app.errorhandler(CSRFError)
def handle_csrf_error(e):
    flash(e.description, 'error')
    return redirect(url_for('index'))


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', menu=buildMenu()), 404


def in_production(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if not app.config['MAINTENANCE_MODE']:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('maintenance'))
    return wrap


@babel.localeselector
def get_locale(user=None):
    if user:
        return user.language
    return request.accept_languages.best_match(app.config['LANGUAGES'].keys())
    

@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index(error=None):
    return render_template('index.html', menu=buildMenu())



@app.route('/test', methods=['GET'])
@in_production
def test(error=None):
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    emails = getCollectiveManagersEmails(con, 'Pioneres')
    print emails


@app.route('/registre', methods=['GET', 'POST'])
@app.route('/registre/<string:preset_collective_name>', methods=['GET', 'POST'])
@in_production
def register(preset_collective_name=None, error=None):
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():

        user = con.search_s(app.config['BASE_DN'], ldap.SCOPE_SUBTREE, '(&(objectClass=inetOrgPerson)(mail=%s))' % form.mail.data)
        if user:
            flash(gettext("An account with this email already exists.") + " <b>%s</b>" % form.mail.data, 'error')
            return redirect(url_for('recoverPassword'))
        
        # Let's register the user here
        user = User(form.name.data, form.surname.data, form.language.data,
                    form.short_bio.data, form.mail.data, form.uid.data, form.primary_collective.data, form.newslettersub)

        user.token = getUniqueToken()
        db.session.add(user)
        db.session.commit()
        sendConfirmEmail(user, request)
        unbind(con)
        return render_template('onboarding-explained.html', user=user, menu=buildMenu())
    
    if preset_collective_name and not form.primary_collective.data:
        collectiveDN = getCollectiveDN(preset_collective_name)
        if collectiveDN:
            collective = doesCollectiveDNExistInDIT(con, collectiveDN)
            if collective:
                collective = get_search_results(collective)[0]
                collectiveCN = collective.get_attr_values('cn')[0]
                form.primary_collective.data = collectiveCN
        
    template = render_template('registre.html', form=form, collectives=json.dumps(getAllCollectivesNames(con)), menu=buildMenu())
    unbind(con)
    return template

@app.route('/manage-services', methods=['GET'])
@manager_required
def listServices(error=None):
    if len(session['managed_services']) == 1:
        return redirect(url_for('groupView', dn=session['managed_services'][0]))

    groups = {}
    for service_dn in session['managed_services']:
        groups[service_dn] = get_rdn(service_dn)
        
    # alphabetical order
    groups = collections.OrderedDict(sorted(groups.items(), key=lambda element: element[0].lower()))
    return render_template('manage_groups.html', group_type="service", groups=groups, menu=buildMenu())


@app.route('/manage-collectives', methods=['GET'])
@manager_required
def listCollectives(error=None):
    if len(session['managed_collectives']) == 1:
        return redirect(url_for('groupView'), dn=session['managed_collectives'][0])
        
    groups = {}
    for collective_dn in session['managed_collectives']:
        groups[collective_dn] = get_rdn(collective_dn)
        
    # alphabetical order
    groups = collections.OrderedDict(sorted(groups.items(), key=lambda element: element[0].lower()))
    return render_template('manage_groups.html', group_type="collective", groups=groups, menu=buildMenu())


@app.route('/manage-users', methods=['GET'])
@manager_required
def listUsers(error=None):
    per_page = 10

    page = request.args.get(get_page_parameter(), type=int, default=1)
    user_query = filter_query(User.query)
    if user_query is None:
        return redirect(url_for('listUsers'))
        
    query = user_query[0]
    search = user_query[1]
    
    users = query.order_by(User.created.desc()).paginate(page,per_page,error_out=False)
    pagination = Pagination(page=page, total=query.count(), search=search, record_name='users')

    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))

    for user in users.items:
        if user.exported:         
            try:
                con.search_s(user.primary_collective_dn, ldap.SCOPE_BASE, '(&(objectclass=groupOfNames)(member=%s))' % user.dn )
                user.does_primary_collective_exist = True
            except:
                user.does_primary_collective_exist = False                

    unbind(con)
    return render_template('manage_users.html',
                           users=users,
                           pagination=pagination,
                           menu=buildMenu()
                           )



@admin_required
@app.route('/update-db', methods=['GET'])
def updateDB():
    users = User.query.all()
    for user in users:
        user.primary_collective = user.primary_collective.replace(',%s' % app.config['COLLECTIVE_OU_DN'], '')
        user.primary_collective = user.primary_collective.replace('cn=', '')
        db.session.commit()
    return render_template('support.html', menu=buildMenu())


"""
Add a new user to the DIT
"""
@app.route('/add-user/<string:id>', methods=['GET', 'POST'])
@manager_required
@in_production
def addUser(id):
    user = User.query.filter_by(id=id).first()
    if not user:
        return redirect(url_for('index'))
    if not canManageUser(user.primary_collective_dn):
        return redirect(url_for('listUsers'))

    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))

    # Lets make sure this dn isn't already in the DIT
    user_ldap_entry = con.search_s( session['baseDN'], ldap.SCOPE_SUBTREE, '(&(objectClass=inetOrgPerson)(uid=%s))' % user.uid.encode("utf-8"))
    if user_ldap_entry:
        user_ldap_entry = get_search_results(user_ldap_entry)[0]          
        unbind(con)
        flash(gettext("User is already in the LDAP DIT"), 'warning')
        return redirect(url_for('userProfile', dn=user_ldap_entry.get_dn()))

    if request.method == 'POST':
        password = generateRandomPassword(12)
        avatar = Avatar(rows=10, columns=10)
        image_byte_array = avatar.get_image(string=generateRandomPassword(12), width=100, height=100, pad=0)
        displayname = '%s %s' % (user.name, user.surname)
        description = 'I am %s. This is my short biography.' % user.short_bio
        add_record = [
            ('objectclass', 'inetorgperson'),
            ('uid', [user.uid.encode("utf-8")]),
            ('userPassword', [password.encode("utf-8")]),
            ('mail', [user.mail.encode("utf-8")]),
            ('cn', [user.name.encode("utf-8")]),
            ('sn', [user.surname.encode("utf-8")]),
            ('displayName', [displayname.encode("utf-8")]),
            ('preferredLanguage', [user.language.encode("utf-8")]),
            ('description', [user.short_bio.encode("utf-8")]),
            ('o', [user.primary_collective.encode("utf-8")]),
            ('jpegPhoto', [image_byte_array] ),
            ('pager', ['250 MB'] )
        ]
        dn = 'uid=%s,%s' % (user.uid, app.config['USER_OU_DN'])
                
        try:
            con.add_s(dn, add_record)
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            unbind(con)
            return redirect(url_for('listUsers'))
                
        user.dn = dn
        user.exported = datetime.datetime.now()
        db.session.commit()
        
        # if the primary_collective does indeed exist in the DIT, let's make this new user a member
        if user.primary_collective_dn in session['managed_collectives']:
            mod_attr = [( ldap.MOD_ADD, 'member', user.dn.encode("utf-8") )]
            try:
                con.modify_s(user.primary_collective_dn, mod_attr)
            except:
                flash("Can't add user to %s" % user.primary_collective_dn, 'warning')
        
        unbind(con)
        return redirect(url_for('userManage', dn=dn))

    if not user.exported:
        not_unique=[]
        # check uid
        if con.search_s( session['baseDN'], ldap.SCOPE_SUBTREE, '(&(objectClass=inetOrgPerson)(uid=%s))' % user.uid.encode("utf-8")):
            not_unique.append(('uid', user.uid))   
        # check if email is unique in DIT
        if con.search_s( session['baseDN'], ldap.SCOPE_SUBTREE, '(&(objectClass=inetOrgPerson)(mail=%s))' % user.mail.encode("utf-8")):
            not_unique.append(('mail', user.mail)) 

        try: 
            if con.search_s( user.primary_collective_dn, ldap.SCOPE_BASE, '(objectClass=groupOfNames)' ):
                collectiveExists = True
        except:
            collectiveExists = False
    
        unbind(con)
        return render_template('adduser.html',  user=user, \
                                                isAdmin=isAdmin(), \
                                                not_unique=not_unique, \
                                                collectiveExists=collectiveExists, \
                                                menu=buildMenu(), \
                                                baseDN=session['baseDN'])
    
    unbind(con)
    return render_template('adduser.html', user=user, menu=buildMenu(), baseDN=session['baseDN'])


"""
admins can edit user info _before_ exporting to ldapserver
"""
@app.route('/edit/<string:id>/<string:attrib>', methods=['GET', 'POST'])
@in_production
@manager_required
def UserEdit(id, attrib):
    user = User.query.filter_by(id=id).first()
    if not user:
        return redirect(url_for('listUsers'))
    request.form.attrib = attrib
    request.form.value = getattr(user, attrib)
    
    if request.method == 'POST':
        value = request.form['value']
        setattr(user, attrib, value)
        if attrib == 'mail':
            user.validated_mail = False
        if attrib == 'uid':
            user.uid = sanitizeUID(user.uid)
        db.session.commit()
        flash(gettext("Updated OK"))
        return redirect(url_for('addUser', id=user.id))
        
    collectives = None
    if attrib == 'primary_collective':
        con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
        if not con:
            return redirect(url_for('add-user', id=id))
        collectives = json.dumps(getAllCollectivesNames(con))
        unbind(con)
    return render_template('edit.html', user=user, collectives=collectives, menu=buildMenu())


"""
Users can modify their ldap attributes. managers and admins can too.
"""
@app.route('/modify/<string:dn>/<string:attribute>', methods=['GET', 'POST'])
@login_required
@in_production
@valid_DN
def modifyAttribute(dn, attribute):        
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    try:
        ldap_user_entry = con.search_s( dn, ldap.SCOPE_BASE, '(objectclass=*)' ) # could improve this search
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        unbind(con)
        return redirect(url_for('index')) 
    ldap_user_entry = get_search_results(ldap_user_entry)[0]
    primary_collective = ldap_user_entry.get_attr_values('o')[0]
    
    primary_collective_dn = "cn=%s,%s" % (primary_collective, app.config['COLLECTIVE_OU_DN'])
    canManage = canManageUser(primary_collective_dn)
    
    if not (canEdit(dn) or canManage):
        unbind(con)
        return redirect(url_for('userProfile', dn=dn))
    
    # only manager and admin can edit these attributes
    if attribute == "pager" and not canManage:
        unbind(con)
        return redirect(url_for('userProfile', dn=dn))

    # only admin can edit these attributes
    if attribute == "o" and not isAdmin():
        unbind(con)
        return redirect(url_for('userProfile', dn=dn))
        
    if request.method == 'POST':
        user = User.query.filter_by(dn=dn).first()  # may return None.
        attrib = escapeLDAP(request.form['attrib'])
        value = escapeLDAP(request.form['value'])
        if not (attrib and value):
            unbind(con)
            return redirect(url_for('userProfile', dn=dn))
        attrib = attrib.encode("utf-8")
        value = value.encode("utf-8")
        mod_attribs=[]

        if attrib == 'cn' or attrib == 'sn':
            if attrib == 'cn':
                dislayname = "%s %s" % (value, ldap_user_entry.get_attr_values('sn')[0].encode('utf-8'))
            if attrib == 'sn':
                dislayname = "%s %s" % (ldap_user_entry.get_attr_values('cn')[0].encode('utf-8'), value)
            mod_attribs.append(( ldap.MOD_REPLACE, 'displayName', dislayname ))
            
        if attrib == 'o':
            #does requested primary_collective exist? If so, add user dn to it.
            if con.search_s(app.config['COLLECTIVE_OU_DN'], ldap.SCOPE_SUBTREE, '(&(objectClass=groupOfNames)(cn=%s))' % str(value)):
                primary_collective_dn = "cn=%s,%s" % (value, app.config['COLLECTIVE_OU_DN'])
                # maybe user is already is a member of the requested primary_collective (although not currently their primary_collective).
                # If so, we should not add them to the groupOfNames because ldap throws an error
                # Check it user_dn is already a member of collective_dn
                if not primary_collective_dn in getCollectiveMembership(con, dn):
                    try:
                        mod_attr = [( ldap.MOD_ADD, 'member', str(dn) )]
                        #print primary_collective_dn
                        con.modify_s(primary_collective_dn, mod_attr)                
                    except ldap.LDAPError, error:
                        flash(unpackError(error), 'error')
                        unbind(con)
                        return redirect(url_for('userManage', dn=dn))
            
            #does original primary_collective exist? If so, remove dn from it.
            #result = con.search_s(dn, ldap.SCOPE_BASE)
            #attribs = get_search_results(ldap_user_entry)[0]
            original_collective = ldap_user_entry.get_attr_values('o')[0]
            collective = con.search_s(app.config['COLLECTIVE_OU_DN'], ldap.SCOPE_SUBTREE, '(&(objectClass=groupOfNames)(cn=%s))' % original_collective)
            if collective:
                attributes = get_search_results(collective)[0]
                attributes = attributes.get_attributes()
                if dn in attributes['member']:
                    # we should check to see if this member is the only member in the groupOfNames because removing the last member throws an error.
                    try:
                        mod_attr = [( ldap.MOD_DELETE, 'member', str(dn) )]
                        original_collective_dn = "cn=%s,%s" % (original_collective, app.config['COLLECTIVE_OU_DN'])
                        con.modify_s(original_collective_dn, mod_attr)
                    except ldap.LDAPError, error:
                        flash(unpackError(error), 'error')
                        unbind(con)
                        return redirect(url_for('userManage', dn=dn))

        mod_attribs.append(( ldap.MOD_REPLACE, attrib, value ))
        try:
            con.modify_s(dn, mod_attribs)
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            flash(gettext("Unable to update"), 'warning')
            unbind(con)
            return redirect(url_for('userProfile', dn=dn))
        
        # do some local sqlite house keeping
        if attrib == 'o':
            if user:
                user.primary_collective = value
                db.session.commit() 
        if attrib == 'mail':
            if user:
                user.mail = value
                db.session.commit()
        if attrib == 'cn':
            if user:
                user.name = value.decode('utf-8')
                db.session.commit()
        if attrib == 'sn':
            if user:
                user.surname = value.decode('utf-8')
                db.session.commit()
        if attrib == 'preferredLanguage':
            if user:
                user.language = value.decode('utf-8')
                db.session.commit()
        if attrib == 'o':
            if user:
                user.primary_collective = value.decode('utf-8')
                db.session.commit()
        
        flash(gettext("Updated OK"))
        unbind(con)
        if dn == session['username']:
            return redirect(url_for('profileAttributes'))
        else:
            return redirect(url_for('userManage', dn=dn))

    # GET    
    collectives = None     
    if attribute == 'o': #only admin can change primary_collective
        collectives = json.dumps(getAllCollectivesNames(con))

    unbind(con)
    #attribs = get_search_results(ldap_user_entry)[0]
    if not ldap_user_entry.has_attribute(attribute):
        error = gettext("Attribute '%s' not found" % attribute)
        flash(unpackError(error), 'error')
        return redirect(url_for('userProfile', dn=dn))
    
    request.form.attrib = attribute
    request.form.value = ldap_user_entry.get_attr_values(attribute)
    return render_template('modify_attribute.html', menu=buildMenu(), attribs=ldap_user_entry, collectives=collectives, dn=dn)


"""
admins can delete a registery petition from the local sqlite database
"""
@app.route('/delete/<string:id>', methods=['GET', 'POST'])
@manager_required
@in_production
def delete(id):
    user = User.query.filter_by(id=id).first()
    if not user:
        flash(gettext("User not found"), 'error')
        return redirect(url_for('listUsers'))
        
    if request.method == 'POST':
        if not user.exported:
            flash(gettext("User deleted"))
            User.query.filter_by(id=id).delete()
            db.session.commit()
        else:
            flash(gettext("Cannot delete. User has been exported to LDAP"), 'error')
        return redirect(url_for('listUsers'))
        
    return render_template('delete_user.html', menu=buildMenu(), user=user)


"""
 Confirm a new user's email, or a changed email 
"""
@app.route('/confirm-email/<string:token>', methods=['GET', 'POST'])
@in_production
def confirmEmail(token):
    user = User.query.filter_by(token=token).first()
    if not user or user.validated_mail:
        return redirect(url_for('index'))

    user.validated_mail = True
    user.token = None
    db.session.commit()

    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])

    accepted_primary_collectives = {'tast-SomNuvol', 'tast-SomosNube'}

    if not con:
        return redirect(url_for('index'))

    if user.exported:
        # update mail attrib on DIT
        attrib =[( ldap.MOD_REPLACE, 'mail', user.mail.encode("utf-8") )]
        try:
            con.modify_s(user.dn, attrib)
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            con.unbind_s()
            return redirect(url_for('index'))
    else:
        if user.primary_collective in accepted_primary_collectives:
            con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
            if not con:
                return redirect(url_for('index'))
            # Create the somnuvol user - BEGIN
            # Lets make sure this dn isn't already in the DIT
            sys.stderr.write("confirmEmail -- session data: %s" % (session))
            user_ldap_entry = con.search_s(app.config['BASE_DN'], ldap.SCOPE_SUBTREE, '(&(objectClass=inetOrgPerson)(uid=%s))' % user.uid.encode("utf-8"))
            #user_ldap_entry = con.search_s( session['baseDN'], ldap.SCOPE_SUBTREE, '(&(objectClass=inetOrgPerson)(uid=%s))' % user.uid.encode("utf-8"))
            if user_ldap_entry:
                user_ldap_entry = get_search_results(user_ldap_entry)[0]
                unbind(con)
                flash(gettext("User is already in the LDAP DIT"), 'warning')
                return redirect(url_for('index'))

            password = generateRandomPassword(12)
            avatar = Avatar(rows=10, columns=10)
            image_byte_array = avatar.get_image(string=generateRandomPassword(12), width=100, height=100, pad=0)
            displayname = '%s %s' % (user.name, user.surname)
            description = 'I am %s. This is my short biography.' % user.short_bio
            add_record = [
                ('objectclass', 'inetorgperson'),
                ('uid', [user.uid.encode("utf-8")]),
                ('userPassword', [password.encode("utf-8")]),
                ('mail', [user.mail.encode("utf-8")]),
                ('cn', [user.name.encode("utf-8")]),
                ('sn', [user.surname.encode("utf-8")]),
                ('displayName', [displayname.encode("utf-8")]),
                ('preferredLanguage', [user.language.encode("utf-8")]),
                ('description', [user.short_bio.encode("utf-8")]),
                ('o', [user.primary_collective.encode("utf-8")]),
                ('jpegPhoto', [image_byte_array] ),
                ('pager', ['250 MB'] )
            ]
            dn = 'uid=%s,%s' % (user.uid, app.config['USER_OU_DN'])

            try:
                con.add_s(dn, add_record)
            except ldap.LDAPError, error:
                flash(unpackError(error), 'error')
                unbind(con)
                return redirect(url_for('index'))

            user.dn = dn
            user.exported = datetime.datetime.now()
            db.session.commit()

            # if the primary_collective does indeed exist in the DIT, let's make this new user a member
            mod_attr = [( ldap.MOD_ADD, 'member', user.dn.encode("utf-8") )]
            try:
                con.modify_s(user.primary_collective_dn, mod_attr)
            except:
                flash("Can't add user to %s" % user.primary_collective_dn, 'warning')

            unbind(con)
            # Create the somnuvol user - END
            # Add the somnuvol user to the somnuvol services - BEGIN
            con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
            if not con:
                return redirect(url_for('index'))

            if user.primary_collective == 'tast-SomosNube':
                somnuvol_mail_service_dn = 'cn=mail-somosnube,ou=serveis,o=femprocomuns,dc=commonscloud,dc=coop'
            else: 
                somnuvol_mail_service_dn = 'cn=mail-somnuvol,ou=serveis,o=femprocomuns,dc=commonscloud,dc=coop'

            somnuvol_oficina_service_dn = 'cn=oficina1,ou=serveis,o=femprocomuns,dc=commonscloud,dc=coop'
            mod_attr = [( ldap.MOD_ADD, 'member', str(user.dn) )]
            try:
                con.modify_s(somnuvol_mail_service_dn, mod_attr)
		con.modify_s(somnuvol_oficina_service_dn, mod_attr)
                print 'Add: %s' % user.dn
            except ldap.LDAPError, error:
                flash(unpackError(error), 'error')
                unbind(con)
                return redirect(url_for('index'))

            # Add the somnuvol user to the somnuvol services - END
            # Final user activation - BEGIN
            user.token = getUniqueToken()
            #user.activated = False
            user.activation_email_sent = datetime.datetime.now()
            sendSetPassword(user, request)
            db.session.commit()
            flash(gettext("Email sent"))
            if (app.config['MAILTRAIN_INTEGRATION']):
                # Mailtrain required newsletter subscription
                mailtrainSubscribeAccount(user, app.config['MAILTRAIN_REQUIRED_LIST_ID'])
                # Mailtrain Somnuvol newsletter subscription
                mailtrainSubscribeAccount(user, app.config['MAILTRAIN_SOMNUVOL_LIST_ID'])
                if (user.newslettersub):
                    # Mailtrain optional newsletter subscription
                    mailtrainSubscribeAccount(user, app.config['MAILTRAIN_OPTIONAL_LIST_ID'])
            # Final user activation - END
            return render_template('onboarding-explained.html', user=user, menu=buildMenu())
        else:
            managers_emails = getCollectiveManagersEmails(con, user.primary_collective)
            sendNewUserNotification(managers_emails, user)

    con.unbind_s()
    if user.activated:
        flash(gettext("Email confirmed OK"))
        return redirect(url_for('login'))

    return render_template('onboarding-explained.html', user=user, menu=buildMenu())
    

"""
An admin can change pre-ldap primary_collective.
And then notify the collective's manager
"""
@app.route('/notify-manager/<string:id>', methods=['POST'])
@admin_required
@in_production
def notifyManager(id):        
    user = User.query.filter_by(id=id).first()
    if not user:
        return redirect(url_for('index'))

    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
        
    managers_emails = getCollectiveManagersEmails(con, user.primary_collective)
    sendNewUserNotification(managers_emails, user)
    flash(gettext("Sent email to manager(s)"))
    
    con.unbind_s()
    return redirect(url_for('addUser', id=id))


"""
A logged in user can change their password
"""
@app.route('/change-password', methods=['GET', 'POST'])
@login_required
@in_production
def changePassword():  
    form = SetPasswordForm(request.form)
    if request.method == 'POST' and form.validate():
        con = bind(session['username'], session['password'])
        if not con:
            return redirect(url_for('index'))
        # update password
        attrib =[( ldap.MOD_REPLACE, 'userPassword', form.password.data.encode("utf-8") )]
        try:
            con.modify_s(session['username'], attrib)
            flash(gettext("Password changed OK"))
            session['password'] = form.password.data.encode("utf-8")
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
        
        unbind(con)
        return redirect(url_for('userProfile'))

    username = None
    user = User.query.filter_by(dn=session['username']).first()
    if user:
        username = user.name
    return render_template('set_password.html', username=username, form=form, menu=buildMenu())


@app.route('/recover-password', methods=['GET', 'POST'])
@in_production
def recoverPassword():
    form = RecoverPasswordForm(request.form)
    if request.method == 'POST' and form.validate():
        mail = escapeLDAP(form.mail.data.encode("utf-8"))
        if not mail:
            return redirect(url_for('index'))

        con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
        if not con:
            return redirect(url_for('index'))
            
        results = con.search_s(app.config['USER_OU_DN'], ldap.SCOPE_SUBTREE, '(&(objectClass=inetOrgPerson)(mail=%s))' % mail)
        unbind(con)

        if not results:
            return redirect(url_for('index'))
        
        attribs = get_search_results(results)[0]
        
        user = User.query.filter_by(dn=attribs.get_dn()).first()
        if user and user.activation_email_sent:
            sys.stderr.write("Password reset email for %s sent to %s" % (user.uid, mail))
            user.token = getUniqueToken()
            sendSetPassword(user, request)
            db.session.commit()
            flash_text = gettext("We've sent you an email")
            flash("%s <i class='fa fa-smile-o' aria-hidden='true'></i>" % flash_text)
            return redirect(url_for('index'))

    return render_template('recover_password.html', form=form, menu=buildMenu())

"""
 A new user is emailed this url to finish onboarding.
 A 'recover password' request is also processed here.
 We make them set a password
"""
@app.route('/reset-password/<string:token>', methods=['GET', 'POST'])
@app.route('/set-password/<string:token>', methods=['GET', 'POST'])
@in_production
def setPassword(token):
    user = User.query.filter_by(token=token).first()
    if not user:
        return redirect(url_for('index'))

    form = SetPasswordForm(request.form)
    if request.method == 'POST' and form.validate():
        con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
        if not con:
            return redirect(url_for('index'))

        # does dn exist in the DIT
        try:
            con.search_s( user.dn, ldap.SCOPE_SUBTREE, '(objectclass=inetOrgPerson)')
        except ldap.NO_SUCH_OBJECT, error:
            flash(unpackError(error), 'error')
            return redirect(url_for('index'))
        # update password
        password = form.password.data.encode("utf-8")
        attrib =[( ldap.MOD_REPLACE, 'userPassword', password )]
        try:
            con.modify_s(user.dn, attrib)
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            unbind(con)
            return redirect(url_for('index'))
            
        user.token = None
        user.validated_mail = True # maybe we continued with onboarding before the user validated. Now we know the email is valid.
        db.session.commit()

        session['password'] = password
        session['username'] = user.dn
        session['baseDN'] = app.config['BASE_DN']
        populateSessionPermissions(con)

        flash(gettext("Your password has been changed successfully"))
        
        if not user.activated: # this is a new user
            user.activated = True
            db.session.commit()
            # websites where this user can login
            websites = []
            results = con.search_s( app.config['BASE_DN'], ldap.SCOPE_SUBTREE, '(&(objectClass=groupOfNames)(member=%s))' % user.dn )
            if results:
                results = get_search_results(results)
                for result in results:
                    if 'businessCategory' in result.get_attr_names():
                        websites.append(result.get_attr_values('businessCategory')[0])
                
            if not app.config['THIS_URL'] in websites:
                # everyone can login here regardless of group membership
                websites.append(app.config['THIS_URL'])
            unbind(con)
            return render_template('onboarding-explained.html', user=user, urls=websites, menu=buildMenu())
        unbind(con)
        return redirect(url_for('userProfile'))

    with force_locale(user.language):
        return render_template('set_password.html', username=user.name, form=form, menu=buildMenu())


@app.route('/profile', methods=['GET'])
@app.route('/profile/<string:dn>', methods=['GET'])
@login_required
def userProfile(dn = None):
    if not dn:
        dn = session['username']
    elif not isValidDN(dn):
        flash(gettext("DN not valid"), 'error')
        return redirect(url_for('index'))

    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    user = User.query.filter_by(dn=dn).first()

    try:
        # get the user from the server
        inetOrgPerson = con.search_s( dn, ldap.SCOPE_SUBTREE, '(objectclass=inetOrgPerson)')
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        if user: # this DN is not in the DIT. Do some local database mantainance.
            user.exported = None
            db.session.commit()
        unbind(con)
        return redirect(url_for('index'))
    
    entry = get_search_results(inetOrgPerson)[0] 
    
    if dn == session['username']:
        websites = getWebSites(con, session['username'])
        groups = session['collectives']
    else:
        # the current user can only see websites they have in common with the queried user(dn)
        websites = getCommonWebSites(con, dn)
        groups = getCommonMembership(con, dn)
    unbind(con)
    
    primary_collective_dn = "cn=%s,%s" % (entry.get_attr_values('o')[0], app.config['COLLECTIVE_OU_DN'])
    can_manage = canManageUser(primary_collective_dn)
    if dn == session['username'] or groups or websites or can_manage:    # current user can query this dn
        return render_template('profile.html',  dn=dn, \
                                                uid=getUID(dn), \
                                                user=user, \
                                                groups=groups, \
                                                websites=websites, \
                                                attribs=entry, \
                                                can_manage=can_manage, \
                                                menu=buildMenu())
    else:
        return redirect(url_for('userProfile'))


@app.route('/profile-attributes', methods=['GET'])
@login_required
def profileAttributes():
    dn = session['username']
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    user = User.query.filter_by(dn=dn).first()

    try:
        # get the user from the server
        entry = con.search_s( dn, ldap.SCOPE_SUBTREE, '(objectclass=inetOrgPerson)')
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        if user: # this DN is not in the DIT. Do some local database mantainance.
            user.exported = None
            db.session.commit()
        unbind(con)
        return redirect(url_for('index'))
    
    entry = get_search_results(entry)[0] 
    unbind(con)
    return render_template('profile_attributes.html', dn=dn, can_edit=canEdit(dn), attribs=entry, menu=buildMenu())


@app.route('/manage-user/<string:dn>', methods=['GET'])
@manager_required
@valid_DN
def userManage(dn):    
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    user = User.query.filter_by(dn=dn).first()
    try:
        # get the user from the server
        inetOrgPerson = con.search_s( dn, ldap.SCOPE_SUBTREE, '(objectclass=inetOrgPerson)')
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        if user: # this DN is not in the DIT. Do some local database mantainance.
            user.exported = None
            db.session.commit()
        unbind(con)
        return redirect(url_for('listUsers'))
    
    entry = get_search_results(inetOrgPerson)[0] 
    primary_collective_dn = "cn=%s,%s" % (entry.get_attr_values('o')[0], app.config['COLLECTIVE_OU_DN'])

    canManage = canManageUser(primary_collective_dn)
    if not canManage:
        return redirect(url_for('index'))

    membership = {}
    membership['Services'] = []
    if session['managed_services']:
        for managedService in session['managed_services']:
            #print managedService
            isMember = False        
            results = con.search_s( managedService, ldap.SCOPE_BASE, '(&(objectClass=groupOfNames)(member=%s))' % dn )
            if results:
                isMember = True            
            membership['Services'].append((managedService, isMember))

    websites = getWebSites(con, dn)
    unbind(con)
    #print membership
    return render_template('manage_user.html',  dn=dn, \
                                                can_edit=canEdit(dn), \
                                                can_manage=canManage, \
                                                can_manage_collective=canManageCollective(primary_collective_dn), \
                                                primary_collective_dn=primary_collective_dn, \
                                                is_admin=isAdmin(), \
                                                user=user, \
                                                queried_groups = json.dumps(membership, ensure_ascii=False), \
                                                websites=websites, \
                                                membership=membership, \
                                                attribs=entry, \
                                                menu=buildMenu() )


'''
Managers can remove users from groups
'''
@app.route('/remove-user/<string:groupDN>/<string:userDN>', methods=['GET','POST'])
@manager_required
@in_production
def userRemove(groupDN, userDN):
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))

    try:
        groupOfNames = con.search_s( groupDN, ldap.SCOPE_BASE, '(objectClass=groupOfNames)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        unbind(con)
        return redirect(url_for('userProfile'))

    groupOfNames = get_search_results(groupOfNames)[0]
    groupInfo = getGroupInfo(groupOfNames)

    if not canManageGroup(groupInfo):
        flash(gettext("Insufficient permission: %s" % groupDN), 'warning')
        unbind(con)
        return redirect(url_for('userProfile'))        
     
    try:
        inetOrgPerson = con.search_s( userDN, ldap.SCOPE_BASE, '(objectClass=inetOrgPerson)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        unbind(con)
        return redirect(url_for('groupView', dn=groupDN))

    inetOrgPerson = get_search_results(inetOrgPerson)[0]
    userAttributes = inetOrgPerson.get_attributes()
    userName = userAttributes['displayName'][0]
    
    primary_collective_dn = "cn=%s,%s" % (userAttributes['o'][0], app.config['COLLECTIVE_OU_DN'])
    if groupDN == primary_collective_dn:
        flash(gettext("Cannot remove user from Primary collective"), 'warning')
        unbind(con)
        return redirect(url_for('groupView', dn=groupDN))
    
    if request.method == 'POST':
        mod_attr = [( ldap.MOD_DELETE, 'member', str(userDN) )]
        try:
            con.modify_s(groupDN, mod_attr)
            flash(gettext("%s removed OK" % userName))
            unbind(con)
            return redirect(url_for('groupView',dn=groupDN))
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
    
    unbind(con)
    return render_template('remove_user.html',  groupInfo=groupInfo, \
                                                userDN=userDN, \
                                                userName=userName, \
                                                menu=buildMenu() )


'''
Managers can add/remove users as members of groupOfNames entries
dn = uid=.....

This is referenced from the manage/uid=.... page
queried_groups comes from the list of checkboxes

'''
@app.route('/update-memberof/<string:userDN>', methods=['POST'])
@manager_required
@in_production
def updateMemberOf(userDN):    
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    
    queried_groups = json.loads(request.form['queried_groups'])
    queried_groups = [item for item in queried_groups.values()]
    queried_groups = [i for sub in queried_groups for i in sub]
    
    current_membership = {}
    for group_dn, membership in queried_groups:
        current_membership[group_dn] = membership

    requested_membership = {}
    for element in request.form:
        if element.endswith(app.config['SERVICE_OU_DN']):
            requested_membership[element]=True

    for group_dn in current_membership:
        if group_dn in requested_membership and requested_membership[group_dn] == current_membership[group_dn]:
            continue
        
        if group_dn in requested_membership and current_membership[group_dn] == False:
            #print 'Add: %s' % group_dn
            mod_attr = [( ldap.MOD_ADD, 'member', str(userDN) )]
            try:
                con.modify_s(group_dn, mod_attr) 
            except ldap.LDAPError, error:
                flash(unpackError(error), 'error')
                unbind(con)
                return redirect(url_for('userManage',dn=userDN))
            
        elif current_membership[group_dn] == True: # was True but now not checked on form (not in requested_membership)
            #print 'Remove: %s' % group_dn
            mod_attr = [( ldap.MOD_DELETE, 'member', str(userDN) )]
            try:
                con.modify_s(group_dn, mod_attr) 
            except ldap.LDAPError, error:
                flash(unpackError(error), 'error')
                unbind(con)
                return redirect(url_for('userManage',dn=userDN))
    
    flash(gettext("Updated OK"))
    unbind(con)
    return redirect(url_for('userManage',dn=userDN))


@app.route('/group/<string:dn>', methods=['GET', 'POST'])
@login_required
@valid_DN
def groupView(dn): 
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    try:
        groupOfNames = con.search_s( dn, ldap.SCOPE_BASE, '(objectClass=groupOfNames)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        unbind(con)
        return redirect(url_for('userProfile'))
        
    groupOfNames = get_search_results(groupOfNames)[0]
    attributes = groupOfNames.get_attributes() 
    groupInfo = getGroupInfo(groupOfNames)
    
    if not canViewGroup(groupInfo):
        flash(gettext("Insufficient permission: %s" % dn), 'warning')
        unbind(con)
        return redirect(url_for('userProfile'))
        
    members = {}
    for member_dn in attributes['member']:
        user = User.query.filter_by(dn=member_dn).first()
        if user:
            members[member_dn] = "%s %s" % (user.name, user.surname)
        else:
            members[member_dn] = member_dn        
    members=collections.OrderedDict(sorted(members.items()))
    
    canManage = canManageGroup(groupInfo)

    if canManage:
        newMember = None
        foundUsers=[]
        if request.method == 'POST':
            if 'search_text' in request.form and request.form['search_text'] and len(request.form['search_text']) > 3:
                # get list of found Users
                users = findExportedUser(request.form['search_text'])
                
                foundDNs=[]
                for user in users:
                    foundUsers.append(user)
                    foundDNs.append(user.dn)
                
                # Perhaps search_text is a uid, and that uid is not in our local sqlite database.
                # So let's look in the DIT for a uid that matches the search_text.
                uid = escapeLDAP(request.form['search_text'])
                if uid:
                    someUserDN = getUserDN(uid)
                    if someUserDN and someUserDN not in foundDNs:
                        # We search in 'USER_OU_DN' but *should change this* if we merge our DIT with another organization's.
                        result = con.search_s( app.config['USER_OU_DN'], ldap.SCOPE_SUBTREE, '(&(uid=%s)(objectclass=inetOrgPerson))' % uid)
                        if result:
                            inetOrgPerson = get_search_results(result)[0]
                            user = User(    name=inetOrgPerson.get_attr_values('cn')[0], \
                                            surname=inetOrgPerson.get_attr_values('sn')[0], \
                                            language='', \
                                            short_bio='', \
                                            mail=inetOrgPerson.get_attr_values('mail')[0], \
                                            uid=inetOrgPerson.get_attr_values('uid')[0], \
                                            primary_collective=inetOrgPerson.get_attr_values('o')[0]
                            
                            )
                            user.dn = inetOrgPerson.get_dn() 
                            print user.name
                            foundUsers.append(user)

                if not foundUsers:
                    flash(gettext("User not found"), 'warning')
                    return redirect(url_for('groupView', dn=dn))
                                        
            elif 'new_member_dn' in request.form:
                new_member_dn = request.form['new_member_dn']
                if not isValidDN(new_member_dn):
                    flash(gettext("DN not valid"), 'error')
                    return redirect(url_for('index'))
                try:
                    result = con.search_s( new_member_dn, ldap.SCOPE_BASE, '(objectclass=inetOrgPerson)')
                    result = get_search_results(result)[0]
                    new_member_dn = result.get_dn() # now we're sure the dn exists in the DIT.
                    displayName = result.get_attr_values('displayName')[0]
                    mod_attr = [( ldap.MOD_ADD, 'member', str(new_member_dn) )]
                    con.modify_s(dn, mod_attr)
                    flash( gettext("Added %s" % displayName), 'message')
                except ldap.LDAPError, error:
                    flash(unpackError(error), 'error')
                    
            # refresh the list of members.
            attributes = con.search_s( dn, ldap.SCOPE_BASE, '(objectClass=groupOfNames)' )
            attributes = get_search_results(attributes)[0]
            attributes = attributes.get_attributes()
            members = {}
            for member_dn in attributes['member']:
                user = User.query.filter_by(dn=member_dn).first()
                if user:
                    members[member_dn] = "%s %s" % (user.name, user.surname)
                else:
                    members[member_dn] = member_dn        
            members=collections.OrderedDict(sorted(members.items()))
    
        unbind(con)
        return render_template('managed_group.html',    dn=dn, \
                                                        cn=getRDN(dn), \
                                                        members=members, \
                                                        canManage=canManage, \
                                                        attribs=attributes, \
                                                        groupInfo=groupInfo, \
                                                        foundUsers=foundUsers, \
                                                        total_members = len(attributes['member']), \
                                                        menu=buildMenu())

    unbind(con)
    return render_template('group.html', \
                            dn=dn, \
                            members=members, \
                            attribs=attributes, \
                            groupInfo=groupInfo, \
                            menu=buildMenu())


@app.route('/create-group/<string:dn>', methods=['GET', 'POST'])
@login_required
@manager_required
def groupCreate(dn):
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    try:
        attributes = con.search_s( dn, ldap.SCOPE_BASE, '(objectClass=*)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        unbind(con)
        return redirect(url_for('groupsList'))
    
    form = GroupForm(request.form)
    if request.method == 'POST' and form.validate():
        try:
            rdn = sanitizeGroupRDN(form.rdn.data)
        except:
            error = "'%s' contains non-ascii character" % form.rdn.data
            flash(unpackError(error), 'error')
            return render_template('group_form.html', menu=buildMenu(), dn=dn, rdn=getRDN(dn), form=form, is_new=True)
                           
        rdn = rdn.encode("utf-8")
        new_group_dn = 'cn=%s,%s' % (rdn, dn)
        
        new_manager_group_dn=None
        if isService(new_group_dn):
            new_manager_group_dn = 'cn=%s,%s' % (rdn, app.config['SERVICE_MANAGER_OU_DN'])
        elif isCollective(new_group_dn):
            new_manager_group_dn = 'cn=%s,%s' % (rdn, app.config['COLLECTIVE_MANAGER_OU_DN'])
            
        new_attributes = [
            ('objectclass', 'groupOfNames'),
            ('cn', rdn),
            ('member', [session['username'].encode("utf-8")]),
            ('description', [form.description.data.encode("utf-8")])
        ]
        # collectives and services will have a corresponding (owner) manager group. Collective sub groups do not.
        if new_manager_group_dn:
            new_attributes.append( ('owner', [new_manager_group_dn]) )
        if form.url.data:
            new_attributes.append( ('businessCategory', [form.url.data.encode('utf-8')]) )
        
        try:
            con.add_s(new_group_dn, new_attributes)
        except ldap.LDAPError, error:
            unbind(con)
            flash(unpackError(error), 'error')
            return render_template('group_form.html', menu=buildMenu(), dn=dn, form=form, is_new=True)
        
        # New collective or Service group has been created. Now automatically create corresponding group of managers.
        if new_manager_group_dn:
            new_attributes = [
                ('objectclass', 'groupOfNames'),
                ('cn', rdn),
                ('member', [session['username'].encode("utf-8")]),
                ('owner', [new_group_dn.encode("utf-8")])
            ]
            try:
                con.add_s(new_manager_group_dn, new_attributes)
            except ldap.LDAPError, error:
                # we should delete new_group_dn here for consistency sake.
                unbind(con)
                flash(_("Failed to create %s" % new_manager_group_dn, 'error'))
                return redirect(url_for('groupView', dn=new_group_dn))
        unbind(con)
        flash(gettext("Group created"))
        return redirect(url_for('groupView', dn=new_group_dn))
            
    unbind(con)
    return render_template('group_form.html', \
                            menu=buildMenu(), \
                            dn=dn, \
                            rdn=getRDN(dn), \
                            form=form, \
                            is_new=True)


@app.route('/manage-groups', methods=['GET'])
@app.route('/manage-groups/<string:collective_dn>', methods=['GET'])
@manager_required
def listCollectiveSubGroup(collective_dn=None):        
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    
    managed_collectives = {}
    for managed_collective in session['managed_collectives']:
        managed_collectives[managed_collective] = getRDN(managed_collective)
    managed_collectives = collections.OrderedDict(sorted(managed_collectives.items(), key=lambda element: element[0].lower()))
    
    if not collective_dn:
        collective_dn = managed_collectives.keys()[0]
    
    try:
        collective_sub_groups = con.search_s( collective_dn, ldap.SCOPE_ONELEVEL, '(objectClass=groupOfNames)' )
        collective_sub_groups = get_search_results(collective_sub_groups)
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        unbind(con)
        return redirect(url_for('index'))
    
    sub_groups={}
    for sub_group in collective_sub_groups:
        sub_groups[sub_group.get_dn()] = {  'rdn':sub_group.get_rdn(), \
                                            'member_count':len(sub_group.get_attr_values('member')), \
                                            'description': sub_group.get_attr_values('description')[0] }
 

    unbind(con)
    
    return render_template('list_collective_sub_groups.html', \
                            menu=buildMenu(), \
                            collectives=managed_collectives, \
                            collective_dn=collective_dn, \
                            canManage=canManageCollective(collective_dn), \
                            collective_rdn=getRDN(collective_dn), \
                            sub_groups=sub_groups )


@app.route('/edit-group/<string:dn>', methods=['GET', 'POST'])
@login_required
@manager_required
@valid_DN
def groupEdit(dn):
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    try:
        groupOfNames = con.search_s( dn, ldap.SCOPE_BASE, '(objectClass=groupOfNames)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        unbind(con)
        return redirect(url_for('listUsers'))

    groupOfNames = get_search_results(groupOfNames)[0]
    attributes = groupOfNames.get_attributes() 
    groupInfo = getGroupInfo(groupOfNames)
    
    if not canManageGroup(groupInfo):
        flash(gettext("Insufficient permission: %s" % dn), 'warning')
        unbind(con)
        return redirect(url_for('listUsers'))
       
    form = GroupForm(request.form)
    if request.method == 'POST' and form.validate():
        mod_attribs=[]

        if form.url.data and not attributes.has_key('businessCategory'):
            mod_attribs.append(( ldap.MOD_ADD, 'businessCategory', form.url.data.encode('utf-8') ))
        elif form.url.data and attributes.has_key('businessCategory'):
            mod_attribs.append(( ldap.MOD_REPLACE, 'businessCategory', form.url.data.encode('utf-8') ))
        elif not form.url.data and attributes.has_key('businessCategory'):
            mod_attribs.append(( ldap.MOD_DELETE, 'businessCategory', attributes.has_key('businessCategory') ))
            
        mod_attribs.append(( ldap.MOD_REPLACE, 'description', form.description.data.encode('utf-8') )) 
        
        try:
            con.modify_s(dn, mod_attribs)
            flash(gettext("Updated OK"))
            unbind(con)
            return redirect(url_for('groupView', dn=dn))
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            flash(gettext("Unable to update"), 'error')
            unbind(con)
            return render_template('group_form.html', \
                                    menu=buildMenu(), \
                                    attribs=attributes, \
                                    dn=dn, \
                                    rdn=getRDN(dn), \
                                    form=form, \
                                    is_new=False)
    unbind(con)
    
    form.description.data = attributes['description'][0]
    if attributes.has_key('businesscategory'):
        form.url.data = attributes['businesscategory'][0]
    return render_template('group_form.html', \
                            menu=buildMenu(), \
                            attribs=attributes, \
                            dn=dn, \
                            rdn=getRDN(dn), \
                            form=form, \
                            is_new=False)


@app.route('/send-group-email/<string:dn>', methods=['GET', 'POST'])
@manager_required
@valid_DN
def groupSendEmail(dn):
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    if not con:
        return redirect(url_for('index'))
    try:
        groupOfNames = con.search_s( dn, ldap.SCOPE_BASE, '(objectClass=groupOfNames)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        unbind(con)
        return redirect(url_for('groupsList'))

    groupOfNames = get_search_results(groupOfNames)[0]
    attributes = groupOfNames.get_attributes() 

    form = GroupEmailForm(request.form)
    if request.method == 'POST' and form.validate():
        bcc=[]
        for member_dn in attributes['member']:
            user = User.query.filter_by(dn=member_dn).first()
            if user:
                bcc.append(user.mail.encode("utf-8"))
            else:
                #fetch info from ldap
                pass
        # added session's user's email to bcc
        user = User.query.filter_by(dn=session['username']).first()
        if user:
            bcc.append(user.mail.encode("utf-8"))
        else:
            #fetch info from ldap
            pass
        
        sendGroupEmail(bcc, form.subject.data, form.body.data)
        flash(gettext("Email sent OK"))
        unbind(con)
        return redirect(url_for('groupView', dn=dn))
        
    unbind(con)
    return render_template('send_group_email.html', menu=buildMenu(), \
                                                    attribs=attributes, \
                                                    groupInfo = getGroupInfo(groupOfNames), \
                                                    dn=dn, \
                                                    total_recipients=len(attributes['member']), \
                                                    form=form)


def mailtrainSubscribeAccount(user, mailtrain_list_id):
    mailtrain_subscribe_url = app.config['MAILTRAIN_URL'] + '/api/subscribe/' + mailtrain_list_id + '?access_token=' + app.config['MAILTRAIN_ACCESS_TOKEN']
    mailtrain_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }
    mailtrain_data = {
        'EMAIL': user.mail,
        'FIRST_NAME': user.name,
        'LAST_NAME': user.surname,
        ('MERGE_'  + user.language.upper()) : 'yes',
        'MERGE_COLLECTIVE': user.primary_collective,
        'REQUIRE_CONFIRMATION': 'no',
    }
    try:
        r = requests.post(mailtrain_subscribe_url, data=mailtrain_data, headers=mailtrain_headers,timeout=4)
        r.raise_for_status()
    except requests.exceptions.RequestException as err:
        # TODO: Show date + time on the logs
        sys.stderr.write ( ("Error connecting to Mailtrain (List id: '%s'): %s\n") % (mailtrain_list_id,err) )
        flash(gettext("Error connecting to Mailtrain. More info on stderr logs. "), 'error')


@app.route('/activate/<string:dn>', methods=['POST'])
@manager_required
@in_production
@valid_DN
def activateAccount(dn): # send activation email    
    user = User.query.filter_by(dn=dn).first()
    if user:        
        user.token = getUniqueToken()
        #user.activated = False
        user.activation_email_sent = datetime.datetime.now()
        sendSetPassword(user, request)
        db.session.commit()
        flash(gettext("Email sent"))
        if (app.config['MAILTRAIN_INTEGRATION']):
            # Mailtrain required newsletter subscription
            mailtrainSubscribeAccount(user, app.config['MAILTRAIN_REQUIRED_LIST_ID'])
            if (user.newslettersub):
                # Mailtrain optional newsletter subscription
                mailtrainSubscribeAccount(user, app.config['MAILTRAIN_OPTIONAL_LIST_ID'])
        return redirect(url_for('userManage', dn=dn))
    else:
        flash(gettext("User not found: %s") % dn, 'error')
        return redirect(url_for('listUsers'))
    

@app.route('/change-photo/<string:dn>', methods=['GET', 'POST'])
@login_required
@in_production
@valid_DN
def changePhoto(dn):
    error = []
    con = bind(session['username'], session['password'])
    if not con:
        return redirect(url_for('index'))
    
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash(gettext("No file part"))
            return redirect(request.url)
        file = request.files['file']
        
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash(gettext("No selected file"), 'error')
            return redirect(request.url)
            
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(filepath)
            # resize image
            photo = Image.open(filepath)
            photo.thumbnail((100, 100), Image.ANTIALIAS)
            # convert to bytearray
            imgByteArr = io.BytesIO()
            photo.save(imgByteArr, format='PNG')
            imgByteArr = imgByteArr.getvalue()
            try:
                con.modify_s( dn, [( ldap.MOD_REPLACE, 'jpegPhoto', imgByteArr )])
            except ldap.LDAPError, error:
                flash(unpackError(error), 'error')
                unbind(con)
                return redirect(url_for('index'))
            flash(gettext("Avatar changed OK"))
            unbind(con)
            return redirect(url_for('userProfile', dn=dn))
    try:
        results = con.search_s( dn, ldap.SCOPE_BASE, '(objectclass=*)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        unbind(con)
        return redirect(url_for('userProfile', dn=dn))
        
    attribs = get_search_results(results)[0]
    request.form.attrib = 'jpegPhoto'
    
    unbind(con)
    return render_template('edit_photo.html', menu=buildMenu(), attribs=attribs, dn=dn, extensions=app.config['PHOTO_EXTENSIONS'])
    

@app.route('/login', methods=['GET', 'POST'])
@in_production
def login():
    session.pop('username', None)
    session.pop('admin', None)
    session.pop('managed_collectives', None)
    session.pop('managed_services', None)
    session.pop('membership', None)
    if request.method == 'POST':
        if not request.form['username'] and not request.form['password']:
            flash(gettext("Credentials needed"), "error")
            return render_template('login.html', menu=buildMenu())

        username = request.form['username'].encode("utf-8")
        
        if not username.endswith(app.config['BASE_DN']):
            # username is not a DN. Maybe it is a UID
            userDN = getUserDN(username)
            if not userDN:
                flash(gettext("Valid credentials needed"), "error")
                return render_template('login.html', menu=buildMenu())

            con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
            if not con:
                return redirect(url_for('index'))

            try:
                results = con.search_s(userDN, ldap.SCOPE_BASE, '(objectClass=inetOrgPerson)')
                if results:
                    attribs = get_search_results(results)[0]
                    username = attribs.get_dn()
            except Exception as error:
                flash(gettext("Valid credentials needed"), "error")
                unbind(con)
                return render_template('login.html', menu=buildMenu())
            unbind(con)
        
        password = request.form['password'].encode("utf-8")
        
        if not isValidDN(username):
            flash(gettext("DN not valid"), 'error')
            return render_template('login.html', menu=buildMenu())
        
        con = bind(username, password)
        if not con:
            return render_template('login.html', menu=buildMenu())
        
        # user creds are correct!
        unbind(con)
        con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
        
        session['username'] = username
        session['password'] = password
        session['baseDN'] = app.config['BASE_DN']
        populateSessionPermissions(con)
                    
        unbind(con)
        if session['managed_collectives']:
            return redirect(url_for('listUsers')) 
        else:
            return redirect(url_for('userProfile'))
    return render_template('login.html', menu=buildMenu())


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    session.pop('username', None)
    session.pop('admin', None)
    session.pop('manager', None)
    session.pop('managed_collectives', None)
    session.pop('managed_services', None)
    session.pop('membership', None)
    return redirect(url_for('index'))


@app.route('/come-back-later', methods=['GET'])
def maintenance():
    if not app.config['MAINTENANCE_MODE']:
        return redirect(url_for('index'))
    return render_template('maintenance.html', menu=buildMenu())


@app.route('/support', methods=['GET'])
def support():
    return render_template('support.html', menu=buildMenu())
